#+title: Unit 8: At the Supermarket
#+author: Jr. 1 Vocabulary
#+EXPORT_FILE_NAME: index

:REVEAL_PROPERTIES:
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: serif
#+OPTIONS: toc:nil timestamp:nil num:nil
:end:

* Parts 1 & 2 Vocab
| vegetable   | beef |
| green beans | taco |
** vegetables (n.)
#+ATTR_HTML: :width 90%
[[file:pics/vegetables.jpg]]
** Green Beans (n.)
#+ATTR_HTML: :width 90%
[[file:pics/green-beans.jpg]]
*** Mung Beans (n.)
#+ATTR_HTML: :width 50%
[[file:pics/Mung-Bean.jpg]]
** taco (n.)
#+ATTR_HTML: :width 60%
[[file:pics/tacos.jpg]]
** Additional Vocab
#+ATTR_HTML: :border 2 :rules all :frame border
| *dairy*         | *meat*       | *soup* |
| stickers        | *vegetables* | math   |
| *grocery store* | *fruit*      | dinner |
| *aromas*        | *fried*      |        |
** dairy (n.)
Milk and food made from milk.
#+ATTR_HTML: :width 80%
[[file:pics/dairy-food.jpg]]
** grocery store (n.)
A store for buying fresh food (only food).
#+ATTR_HTML: :width 90%
[[file:pics/grocery-store.jpg]]
** aromas (n.)
The smells coming from something.
#+ATTR_HTML: :width 90%
[[file:pics/aromas.jpg]]
** meat (n.)
#+ATTR_HTML: :width 90%
[[file:pics/meat.jpg]]
** fruit (n.)
#+ATTR_HTML: :width 90%
[[file:pics/fruits.jpg]]
** fried (adj.)
Cooked in hot oil.
#+ATTR_HTML: :width 80%
[[file:pics/fried-foods.jpg]]
** soup (n.)
#+ATTR_HTML: :width 90%
[[file:pics/cabbage-soup.jpg]]
* Part 4 & 5 Vocab
#+ATTR_HTML: :border 2 :rules all :frame border
| typical           | label      | calculator |
| habit             | videotape  | produce    |
| source            | aisle      | technology |
| facial expression | department | investment |
** typical (adj.)
Has all the usual parts of something.
#+ATTR_HTML: :width 70%
[[file:pics/taiwan-breakfast.jpg]]
** labels (n.)
#+ATTR_HTML: :width 80%
[[file:pics/food-labels.jpg]]
** calculator (n.)
#+ATTR_HTML: :width 80%
[[file:pics/calculator.webp]]
** produce (n.)
Food that grows (fruits and vegetables).
[[file:pics/produce.jpg]]
** facial expressions (n.)
[[file:pics/facial-expressions.jpg]]
** videotape (n.)
#+ATTR_HTML: :width 80%
[[file:pics/videotape.jpg]]
** videotape (v.)
#+ATTR_HTML: :width 80%
[[file:pics/security-camera.jpg]]
** source (n.)
Where things come from.
#+ATTR_HTML: :width 40%
[[file:pics/essential-oil-diffuser.jpg]]

/The _source_ of the smell./
** aisle (n.)
The space between the shelves in a store.
#+ATTR_HTML: :width 80%
[[file:pics/aisle.jpg]]
** department (n.)
One part of a store, usually with one product.
#+ATTR_HTML: :width 55%
[[file:pics/produce-department.jpg]]
** investment (n.)
Spending money now, to make more money later.
#+ATTR_HTML: :width 80%
[[file:pics/investing.jpg]]
** Other Vocab
#+ATTR_HTML: :border 2 :rules all :frame border
| grocery shoppers | daily          | list               |
| market           | fresh          | deal               |
| pound            | aerator        | marketing strategy |
| pasta            | smells/ scents | supermarket        |
| bakery           | section        | grapefruit         |
** Other Vocab
#+ATTR_HTML: :border 2 :rules all :frame border
| cost             | pumped         | possibility        |
| frown            | smoked meat    | interpreting       |
| deny             | intense        | mood               |
|                  | merchandise    | coordinator        |
** grocery shopper (n.)
A person buying groceries (uncooked food).
#+ATTR_HTML: :width 70%
[[file:pics/grocery-shopper.jpg]]
** list (n.)
#+ATTR_HTML: :width 80%
[[file:pics/shopping-list.jpg]]
** market (n.)
#+ATTR_HTML: :width 80%
[[file:pics/morning-market.jpg]]
** marketing strategy (n.)
A plan for how to get people to buy your products.
[[file:pics/business-plan.jpg]]
* Part 6 & WB Vocab
#+ATTR_HTML: :border 2 :rules all :frame border
| chef       | recipe     | ingredients |
| omelet     | convenient | deliver     |
| convenient | research   | junk food   |
** cook / chef (n.)
[[file:pics/chef.jpg]]
** ingredients (n.)
[[file:pics/ingredients.jpg]]
** omelet (n.)
[[file:pics/omelette.jpg]]
** recipe (n.)
[[file:pics/smoothie-recipe.png]]
** convenient (adj.)
[[file:pics/seven-eleven.jpg]]
** deliver (v.)
[[file:pics/uber-eats.jpg]]
** research (v.)
Study and learn about a topic.
#+ATTR_HTML: :width 85%
[[file:pics/research.jpg]]
** junk food (n.)
#+ATTR_HTML: :width 85%
[[file:pics/junk-food.jpg]]
** Other Vocab
#+ATTR_HTML: :border 2 :rules all :frame border
| green pepper | delicious       | *dish=meal* |
| onion        | congratulations | engage      |
| secret       | involve         | audience    |
| activity     | guess           | classmate   |
** Other Vocab
#+ATTR_HTML: :border 2 :rules all :frame border
| blog post | *refrigerator* | busy   |
| order     | saves          | chips  |
| package   | smart phone    | shrimp |
| parent    | groceries      | duck   |
** dish / meal (n.)
[[file:pics/meal-dish.jpg]]
** refrigerator (n.)
#+ATTR_HTML: :width 45%
[[file:pics/refridgerator.jpg]]
*  Food Around the world
| Brazil   | - | feijoada     | Britain   | - | shepherd's pie |
| /        |   |              | <         |   | >              |
| China    | - | Peiking duck | Indonesia | - | rendang        |
| Iran     | - | kebab        | Korea     | - | bibimbap       |
| Mexico   | - | taco         | Spain     | - | paella         |
| Thailand | - | curry        | Japan     | - | sushi          |
** feijoada
#+ATTR_HTML: :width 70%
[[file:pics/Feijoada.webp]]
** shepherd's pie
#+ATTR_HTML: :width 60%
[[file:pics/shepherds-pie.webp]]
** peiking duck
[[file:pics/peiking-duck.jpg]]
** rendang
[[file:pics/beef-rendang.jpg]]
** kebab
#+ATTR_HTML: :width 75%
[[file:pics/kebab.jpg]]
** bibimbap
#+ATTR_HTML: :width 80%
[[file:pics/bibimbap.webp]]
** taco
#+ATTR_HTML: :width 65%
[[file:pics/tacos.jpg]]
** paella
#+ATTR_HTML: :width 55%
[[file:pics/paella.jpg]]
** curry
#+ATTR_HTML: :width 65%
[[file:pics/thai-red-curry.jpg]]
** sushi
#+ATTR_HTML: :width 85%
[[file:pics/sushi.webp]]
